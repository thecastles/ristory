# Ristory

An enhanced replacement for history command.

## Testing

### Coverage

To produce coverage output files in the converage folder, run

```bash

cargo kcov --output coverage --lib -- --exclude-pattern=include,snap,.cargo

```
