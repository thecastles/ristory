//! Ristory - An enhanced history application
//!
//! Ristory provides the same functionality that the
//! `history` command does, with some extra niceties
//! and enhancements, e.g., fuzzy search and automatic
//! execution of commands.
mod file_struct;
use file_struct::get_history_file;

/// Entry point of the program.
fn main() {
    println!("History folder is {}", get_history_file());
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_main() {
        main()
    }
}
