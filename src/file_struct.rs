use std::path::PathBuf;

const BASH_HISTORY: &str = ".bash_history";

pub fn get_home_dir() -> Result<PathBuf, String> {
    let home_dir = home::home_dir();
    match home_dir {
        Some(folder) => Ok(folder),
        None => Err(String::from("Home folder not set")),
    }
}

pub fn get_history_file() -> String {
    let home_dir = get_home_dir();
    let full_path: PathBuf = [home_dir.unwrap(), PathBuf::from(BASH_HISTORY)]
        .iter()
        .collect();

    String::from(full_path.to_str().unwrap_or_default())
}

#[cfg(test)]
mod tests {

    use super::*;
    use std::fs;
    use std::io::Error;

    #[test]
    fn test_home_dir() {
        let folder = get_home_dir().unwrap();
        assert!(folder.starts_with("C:\\Users") || folder.starts_with("/home"));
    }

    #[test]
    fn test_read_history() {
        let history_file = get_history_file();
        let contents: Result<String, Error> = fs::read_to_string(history_file);
        match contents {
            Ok(content) => assert!(content.len() > 0),
            Err(error) => panic!("Error found when reading the file: {}", error),
        }
    }

    #[test]
    fn test_no_history_file() {
        let history_file = "/.bash_history";
        let contents: Result<String, Error> = fs::read_to_string(history_file);
        if let Ok(_) = contents {
            panic!("Home folder should have been absent");
        }
    }
}
